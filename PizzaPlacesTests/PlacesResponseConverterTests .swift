//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import XCTest
import CoreLocation
@testable import PizzaPlaces

class PlacesResponseConverterTests: XCTestCase {

    func testConversion() {
        // Given
        let placeApi = PlaceApi(id: "id",
                                name: "name",
                                phone: "phone",
                                website: "website",
                                formattedAddress: "address",
                                city: "city",
                                openingHours: ["hours"],
                                longitude: 0.0,
                                latitude: 1.0,
                                images: [.init(id: "id",
                                               url: "google.com",
                                               caption: "",
                                               expiration: "")],
                                friendIds: ["1", "3"])
        let friendsApi: [FriendApi] = [.init(id: 1, name: "1", avatarUrl: "apple.com"),
                                       .init(id: 2, name: "", avatarUrl: ""),
                                       .init(id: 3, name: "3", avatarUrl: "")]
        // When
        let converted = PlacesResponseConverter().convert(apiPlaces: [placeApi],
                                                          apiFriends: friendsApi)
        // Then
        guard let place = converted.first else {
            XCTFail("Should be only one place")
            return
        }
        XCTAssertEqual(place, Place(id: "id",
                                    name: "name",
                                    phone: "phone",
                                    website: "website",
                                    formattedAddress: "address",
                                    city: "city",
                                    openingHours: ["hours"],
                                    location: CLLocationCoordinate2D(latitude: 1.0, longitude: 0.0),
                                    imageUrls: [URL(string: "google.com")!],
                                    friends: [Friend(id: "1",
                                                     name: "1",
                                                     avatarUrl: URL(string: "apple.com")!),
                                              Friend(id: "3",
                                                     name: "3",
                                                     avatarUrl: nil)]))
    }
}
