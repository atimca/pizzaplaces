# PizzaPlaces

There are also three examples of my code:  
1) [With architectural approach and clean code](https://github.com/Atimca/Currencies)  
2) [Regarding to one screen task with the `KISS` principle](https://github.com/Atimca/PhotoGallery)  
3) [Just a task written on Objective-C](https://github.com/Atimca/EmployeeSalaries)

**Task:**

Develop a small prototype that can show a list of the best pizza places in town in a map and its details.
This prototype will consist of:

- First screen: A map using Google Maps focused of the pizza places. You can display a thumbnail, the name of the restaurant and whether its open on the info window after clicking on the marker
- Second screen: Detail screen with a big image, and options to vote, mark as favorite and Book a Table. Show also a list of which of your friends have been there. The action buttons can be mocked.

**Tech stack:**

- Plain `URLSession` for networking
- `DataDriven` pattern for working with views
- `Kingfisher` for image downloading
- `RxFeedback` acrhitecture pattern as base

**Disclamer:**

I completely understand that RxFeedback is some kind of overengineering for this task. However, I wanted this test to be a demonstration of abilities and not just the simplest solution for the app that only contains two screens. Also, I've cut some edges. I created details screen without connection to RxFeedback  because the idea should be understandable on the main screen. I didn't do proper testing, because you can see it in another example.
PS: I've designed it by myself and added Sketch file ;)