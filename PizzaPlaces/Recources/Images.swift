//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import UIKit

enum Images {
    static let bookbarkOn = #imageLiteral(resourceName: "bookmark_on")
    static let bookmarkOff = #imageLiteral(resourceName: "bookmark_off")
    static let friendPlaceholder = #imageLiteral(resourceName: "friend_placeholder")
    static let pizzaIcon = #imageLiteral(resourceName: "pizzaIcon")
    static let buttonBackgroundNormal = #imageLiteral(resourceName: "button_background_normal")
    static let buttonBackgroundHighlighted = #imageLiteral(resourceName: "button_background_highlighted")
}
