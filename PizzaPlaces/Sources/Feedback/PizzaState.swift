//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct PizzaState: Equatable {
    typealias Event = PizzaEvent

    var places: [Place] = []
    var selectedPlace: Place?
}

enum PizzaEvent {
    case placeSelected(location: CLLocationCoordinate2D)
    case placesWereDownloaded([Place])
    case detailsLoaded
}

// MARK: - Query
extension PizzaState {
    var queryShouldDownloadPlaces: Bool {
        guard places.isEmpty else { return false }
        return true
    }
}

// MARK: - Reducer
extension PizzaState {
    static func reduce(state: PizzaState, event: PizzaEvent) -> PizzaState {
        var result = state
        switch event {
        case .placeSelected(let location):
            result.selectedPlace = result.places.first { $0.location == location }
        case .placesWereDownloaded(let places):
            result.places = places
        case .detailsLoaded:
            result.selectedPlace = nil
        }
        return result
    }
}
