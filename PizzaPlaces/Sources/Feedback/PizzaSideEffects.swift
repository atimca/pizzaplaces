//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import RxSwift
import RxFeedback

typealias PizzaFeedbackLoop = (ObservableSchedulerContext<PizzaState>) -> Observable<PizzaEvent>

struct PizzaSideEffects {

    private let coordinator: Coordinator
    private let placesService: PlacesFacade

    public init(coordinator: Coordinator, placesService: PlacesFacade) {
        self.coordinator = coordinator
        self.placesService = placesService
    }
}

extension PizzaSideEffects {
    var feedbackLoops: [PizzaFeedbackLoop] {
        return [
            react(request: { $0.selectedPlace }, effects: openDetailsScene),
            react(request: { $0.queryShouldDownloadPlaces }, effects: downloadPlaces)
        ]
    }

    var openDetailsScene: (_ place: Place) -> Observable<PizzaEvent> {
        return {
            self.coordinator.showDetails(with: $0)
            return .just(.detailsLoaded)
        }
    }

    var downloadPlaces: (Bool) -> Observable<PizzaEvent> {
        return {
            guard $0 else { return .empty() }
            return self.placesService.getPlaces()
                .map { .placesWereDownloaded($0) }
                .catchError({ _ in return .just(.placesWereDownloaded([])) })
                .asObservable()
        }
    }
}
