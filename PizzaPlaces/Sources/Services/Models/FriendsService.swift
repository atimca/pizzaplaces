//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation
import RxSwift

struct FriendsService {

    private let networkClient: NetworkClient

    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
    }
}

extension FriendsService {
    func getFriends() -> Single<[FriendApi]> {
        return networkClient.performGet(endpoint: Constants.endpoint, for: [FriendApi].self)
    }
}

private extension FriendsService {
    enum Constants {
        static let endpoint = URL(string: "https://demo4327201.mockable.io/pizza-api/friends")!
    }
}
