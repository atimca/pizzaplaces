//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct PlacesResponse: Codable {
    let list: ListAPI
}

struct ListAPI: Codable {
    let places: [PlaceApi]
}
