//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct ImageApi: Codable {
    let id: String
    let url: String
    let caption: String
    let expiration: String
}
