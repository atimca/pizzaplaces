//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct FriendApi: Codable {
    let id: Int
    let name: String
    let avatarUrl: String
}
