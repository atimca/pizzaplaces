//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct PlaceApi: Codable {
    let id: String
    let name: String
    let phone: String?
    let website: String
    let formattedAddress: String
    let city: String
    let openingHours: [String]
    let longitude: Double
    let latitude: Double
    let images: [ImageApi]
    let friendIds: [String]
}
