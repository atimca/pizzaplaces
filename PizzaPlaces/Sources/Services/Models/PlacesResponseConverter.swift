//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct PlacesResponseConverter {
    func convert(apiPlaces: [PlaceApi], apiFriends: [FriendApi]) -> [Place] {
        return apiPlaces.map { convert(apiPlace: $0, apiFriends: apiFriends) }
    }

    private func convert(apiPlace: PlaceApi, apiFriends: [FriendApi]) -> Place {

        let apiFriends = apiFriends.filter { apiPlace.friendIds.contains("\($0.id)") }

        return Place(id: apiPlace.id,
                     name: apiPlace.name,
                     phone: apiPlace.phone,
                     website: apiPlace.website,
                     formattedAddress: apiPlace.formattedAddress,
                     city: apiPlace.city,
                     openingHours: apiPlace.openingHours,
                     location: CLLocationCoordinate2D(latitude: apiPlace.latitude, longitude: apiPlace.longitude),
                     imageUrls: apiPlace.images.compactMap { URL(string: $0.url) },
                     friends: apiFriends.map(Friend.init))
    }
}

private extension Friend {
    init(from apiFriend: FriendApi) {
        self.init(id: "\(apiFriend.id)",
            name: apiFriend.name,
            avatarUrl: URL(string: apiFriend.avatarUrl))
    }
}
