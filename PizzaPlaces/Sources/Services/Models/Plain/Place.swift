//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct Place: Equatable {
    let id: String
    let name: String
    let phone: String?
    let website: String
    let formattedAddress: String
    let city: String
    let openingHours: [String]
    let location: CLLocationCoordinate2D
    let imageUrls: [URL]
    let friends: [Friend]
}
