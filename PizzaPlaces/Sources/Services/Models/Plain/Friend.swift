//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

struct Friend: Codable, Equatable {
    let id: String
    let name: String
    let avatarUrl: URL?
}
