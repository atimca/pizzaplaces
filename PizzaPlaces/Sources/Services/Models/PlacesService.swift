//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation
import RxSwift

struct PlacesService {

    private let networkClient: NetworkClient

    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
    }
}

extension PlacesService {
    func getPlaces() -> Single<PlacesResponse> {
        return networkClient.performGet(endpoint: Constants.endpoint, for: PlacesResponse.self)
    }
}

private extension PlacesService {
    enum Constants {
        static let endpoint = URL(string: "https://demo4327201.mockable.io/pizza-api/pizzaplaces")!
    }
}
