//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import UIKit

extension UINavigationBar {
    func setDefault() {
        tintColor = .orange
        isTranslucent = false
        barStyle = .black
        titleTextAttributes = [.foregroundColor: UIColor.white]
    }
}
