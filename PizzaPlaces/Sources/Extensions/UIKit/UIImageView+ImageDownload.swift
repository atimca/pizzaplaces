//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Kingfisher
import UIKit

extension UIImageView {
    func setImage(with url: URL?, completion: (() -> Void)? = nil) {
        kf.setImage(with: url) { _ in completion?() }
    }
}
