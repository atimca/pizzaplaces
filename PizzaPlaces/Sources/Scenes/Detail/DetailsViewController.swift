//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import RxCocoa
import RxSwift
import UIKit

class DetailsViewController: UIViewController {

    // MARK: - Properties

    private let viewState: ViewState
    private let disposeBag = DisposeBag()

    // MARK: - UI Componets

    private let imagePlaceholder: UIImageView = {
        let view = UIImageView(image: Images.pizzaIcon)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 80).isActive = true
        view.heightAnchor.constraint(equalToConstant: 80).isActive = true
        view.tintColor = #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1)
        return view
    }()

    private let imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 0.3)
        view.widthAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1.5).isActive = true
        return view
    }()

    private let bookButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(Images.buttonBackgroundNormal, for: .normal)
        button.setBackgroundImage(Images.buttonBackgroundHighlighted, for: .highlighted)
        button.clipsToBounds = true
        button.layer.cornerRadius = 10
        button.titleLabel?.font = .systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Book now", for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return button
    }()

    private let markButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Images.bookmarkOff, for: .normal)
        return button
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .body)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()

    private var friendsImageViews: [UIImageView] = {
        (0...3).map { _ in
            let view = UIImageView(image: Images.friendPlaceholder)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.widthAnchor.constraint(equalToConstant: 40).isActive = true
            view.heightAnchor.constraint(equalToConstant: 40).isActive = true
            view.layer.cornerRadius = 20
            view.clipsToBounds = true
            return view
        }
    }()

    // MARK: - Life Cycle

    init(place: Place) {
        self.viewState = .init(from: place)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        setupEventHandling()
        render(with: viewState)
    }

    private func render(with state: ViewState) {
        title = state.title

        imageView.setImage(with: state.logoUrl) { [weak self] in
            self?.imagePlaceholder.isHidden = true
        }

        state.friendsImageUrls.suffix(friendsImageViews.count).enumerated().forEach {
            friendsImageViews[$0.offset].setImage(with: $0.element)
        }

        descriptionLabel.text = state.description
    }

    private func setupUi() {
        view.backgroundColor = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.setDefault()
        view.addSubview(imageView)
        imageView.addSubview(imagePlaceholder)

        imagePlaceholder.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        imagePlaceholder.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true

        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose,
                                                            target: self,
                                                            action: nil)

        let stack = UIStackView(arrangedSubviews: friendsImageViews)
        view.addSubview(stack)
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.spacing = 16
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16).isActive = true
        stack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true

        view.addSubview(markButton)
        markButton.centerYAnchor.constraint(equalTo: stack.centerYAnchor).isActive = true
        markButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true

        view.addSubview(descriptionLabel)
        descriptionLabel.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 16).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true

        view.addSubview(bookButton)
        bookButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                           constant: -24).isActive = true
        bookButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        bookButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40).isActive = true
    }

    private func setupEventHandling() {

        markButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                if self.markButton.currentImage == Images.bookbarkOn {
                    self.markButton.setImage(Images.bookmarkOff, for: .normal)
                } else {
                    self.markButton.setImage(Images.bookbarkOn, for: .normal)
                }
            }
            .disposed(by: disposeBag)

        bookButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                let alertController = UIAlertController(title: "Book mock",
                                                        message: nil,
                                                        preferredStyle: .alert)
                alertController.addAction(.init(title: "Ok", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            .disposed(by: disposeBag)

        navigationItem.rightBarButtonItem?.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                let alertController = UIAlertController(title: "Vote mock",
                                                        message: nil,
                                                        preferredStyle: .alert)
                alertController.addAction(.init(title: "Ok", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            .disposed(by: disposeBag)
    }
}
