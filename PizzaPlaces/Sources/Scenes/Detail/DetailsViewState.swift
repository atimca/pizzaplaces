//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation

extension DetailsViewController {
    struct ViewState {
        let title: String
        let logoUrl: URL?
        let friendsImageUrls: [URL]
        let description: String
    }
}

extension DetailsViewController.ViewState {
    init(from place: Place) {
        self.init(title: place.name,
                  logoUrl: place.imageUrls.first,
                  friendsImageUrls: place.friends.compactMap { $0.avatarUrl },
                  description: place.city + "\n" +
                    place.formattedAddress + "\n" +
                    (place.phone ?? ""))
    }
}
