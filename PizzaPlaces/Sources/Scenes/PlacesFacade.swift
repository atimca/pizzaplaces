//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import Foundation
import RxSwift

struct PlacesFacade {

    private let placesService: PlacesService
    private let friendsService: FriendsService
    private let converter: PlacesResponseConverter

    init(placesService: PlacesService,
         friendsService: FriendsService,
         converter: PlacesResponseConverter) {
        self.placesService = placesService
        self.friendsService = friendsService
        self.converter = converter
    }
}

extension PlacesFacade {
    func getPlaces() -> Single<[Place]> {

        return Observable
            .combineLatest(placesService.getPlaces().asObservable(),
                           friendsService.getFriends().asObservable())
            .map { placesResponse, apiFriends in
                self.converter.convert(apiPlaces: placesResponse.list.places,
                                       apiFriends: apiFriends)
            }.asSingle()
    }
}
