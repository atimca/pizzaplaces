//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import UIKit

enum ControllerFactory {
    static var makeMapController: MapViewController {
        return MapViewController()
    }

    static func makeDetailsController(with place: Place) -> UIViewController {
        return DetailsViewController(place: place)
    }
}
