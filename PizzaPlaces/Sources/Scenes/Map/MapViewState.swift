//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import CoreLocation

struct PizzaPointViewState {
    let id: String
    let name: String
    let openHours: String
    let location: CLLocationCoordinate2D
}

extension MapViewController {
    struct ViewState {
        let pizzaPoints: [PizzaPointViewState]

        static func convert(from places: [Place]) -> ViewState {
            return ViewState(pizzaPoints: places.map(PizzaPointViewState.init))
        }
    }
}

private extension PizzaPointViewState {
    init(from place: Place) {
        self.init(id: place.id,
                  name: place.name,
                  openHours: place.openingHours.reduce("") { $0 + "\n" + $1 },
                  location: place.location)
    }
}
