//
// Copyright © 2019 Smirnov Maxim. All rights reserved. 
//

import GoogleMaps
import RxFeedback
import RxSwift
import UIKit
import RxGoogleMaps

class MapViewController: UIViewController {

    // MARK: - UI Components

    private let mapView: GMSMapView = {
        let map = GMSMapView()
        // swiftlint:disable force_try
        let styleURL = Bundle.main.url(forResource: "style", withExtension: "json")!
        map.mapStyle = try! GMSMapStyle(contentsOfFileURL: styleURL)
        // swiftlint:enable force_try
        return map
    }()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Life Cycle

    override func loadView() {
        view = mapView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Main Screen"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

// MARK: - DataDriven
extension MapViewController {
    func render(state: ViewState) {
        state.pizzaPoints.forEach { place in
            let marker = GMSMarker(position: place.location)
            marker.map = mapView
            marker.icon = Images.pizzaIcon
            marker.title = place.name
            marker.snippet = place.openHours
        }
        guard let location = state.pizzaPoints.first?.location else { return }
        mapView.moveCamera(.setTarget(location, zoom: 12.0))
    }
}

// MARK: - Bindings
extension MapViewController {
    var uiFeedback: PizzaFeedbackLoop {
        return bind(self) { vc, state in
            let viewState = state
                .map { $0.places }
                .distinctUntilChanged()
                .map(ViewState.convert)
                .asDriver(onErrorDriveWith: .empty())

            let subscriptions = [
                viewState
                    .drive(onNext: { vc.render(state: $0) })
            ]

            let events: [Observable<PizzaEvent>] = [
                vc.mapView.rx.didTapInfoWindowOf
                    .map { .placeSelected(location: $0.position) }
            ]
            return Bindings(subscriptions: subscriptions, events: events)
        }
    }
}
